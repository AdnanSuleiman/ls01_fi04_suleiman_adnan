package me.Adnan.KonsolenAusgabe;

public class Fakultaet {
	
	public static void main(String[] args) {
		
		int i = 0;
		
		System.out.printf("%-5s", i + "!").printf("%-19s", "=").printf("%s", " = ").printf("%4s", "1" + "\n");
		i++;
		
		System.out.printf("%-5s", i + "!").printf("%-19s", "= 1").printf("%s", " = ").printf("%4s", "1" + "\n");
		i++;
		
		System.out.printf("%-5s", i + "!").printf("%-19s", "= 1 * 2").printf("%s", " = ").printf("%4s", "2" + "\n");
		i++;
		
		System.out.printf("%-5s", i + "!").printf("%-19s", "= 1 * 2 * 3").printf("%s", " = ").printf("%4s", "6" + "\n");
		i++;
		
		System.out.printf("%-5s", i + "!").printf("%-19s", "= 1 * 2 * 3 * 4").printf("%s", " = ").printf("%4s", "24" + "\n");
		i++;
		
		System.out.printf("%-5s", i + "!").printf("%-19s", "= 1 * 2 * 3 * 4 * 5").printf("%s", " = ").printf("%4s", "120" + "\n");
		
	}

}
