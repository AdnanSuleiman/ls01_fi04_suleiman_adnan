package me.Adnan.KonsolenAusgabe;

public class TemepraturTabelle {
	
	public static void main(String[] args) {
		
		System.out.printf("%-11s", "Fahrenheit").printf("%s", "|").printf("%11s", "Celsius").printf("%s", "\n");

		for (int i = 0; i < 23; i++) {
			System.out.printf("%s", "-");
		}
		
		System.out.printf("%-12s", "\n-20").printf("%s", "|").printf("%11.2f", -28.8889);
		System.out.printf("%-12s", "\n-10").printf("%s", "|").printf("%11.2f", -23.3333);
		System.out.printf("%-12s", "\n+0").printf("%s", "|").printf("%11.2f", -17.7778);
		System.out.printf("%-12s", "\n+20").printf("%s", "|").printf("%11.2f", -6.6667);
		System.out.printf("%-12s", "\n+30").printf("%s", "|").printf("%11.2f", -1.1111);
		
		//Lösungsvorschlag
//		System.out.printf("%-12s|%10s\n", "Fahrenheit", "Celsius");
//		
//		for (int i = 0; i < 23; i++) {
//			System.out.printf("%s", "-");
//		}
//		
//		System.out.printf("%-12s|%10s\n", "-20", "-28.89");
//		System.out.printf("%-12s|%10s\n", "-10", "-23.33");
//		System.out.printf("%-12s|%10s\n", "+0", "-17.78");
//		System.out.printf("%-12s|%10s\n", "+20", "-6.67");
//		System.out.printf("%-12s|%10s\n", "+30", "-1.11");
	}
	
}
