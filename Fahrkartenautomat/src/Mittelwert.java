import java.util.Scanner;

public class Mittelwert {

   public static void main(String[] args) {
	   Scanner input = new Scanner(System.in);
	   
      // (E) "Eingabe"
      // Werte f�r x und y festlegen:
      // ===========================
	   
	   System.out.println("Wie viele Werte wollen Sie eingeben?");
	   
	   int anzahlWerte = input.nextInt();
	   double akkumulator = 0;
	   double[] zahlen = new double[anzahlWerte]; //Double hat genauso viele Werte, wie Nutzer sie eingegeben hat
	   
	   for(int i = 0; i < anzahlWerte; i++) {
		   System.out.println("Bitte geben Sie einen Wert ein:");
		   
		   zahlen[i] = input.nextDouble(); 
		   
		   akkumulator += zahlen[i];
		   
	   }
	      
	   double m;
      
      // (V) Verarbeitung
      // Mittelwert von x und y berechnen: 
      // ================================
      m = akkumulator / anzahlWerte;
      
      // (A) Ausgabe
      // Ergebnis auf der Konsole ausgeben:
      // =================================
      System.out.printf("Der Mittelwert ist %.2f\n", m);
      
      for(int i = 0; i < zahlen.length; i++) {
    	  System.out.print(zahlen[i] + ", ");
      }
      
      //System.out.println(zahlen[200]); 
   }
   
   public static double liesDoubleWertEin(String frage) {
	   double zahl;
	   
	   Scanner input = new Scanner(System.in);
	   System.out.println(frage);
	   
	   zahl = input.nextDouble();
	   
	   return zahl;
   }
   
   private static double mittelwertBerechnen(double x, double y) {
	   double m;
	   m = (x + y) / 2.0;
	   return m;
   }
}
