import java.util.Scanner;

public class Folgen {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		System.out.println("Gebe eine Zahl ein: ");
		int number = input.nextInt();

		//Aufgabe a.)
		System.out.println("Aufgabe A: ");
		for(int i = number; i > 9; i -= 3) {
			System.out.print(i + ", ");
		}

		System.out.println("");
		
		//Aufgabe b.)
		System.out.println("Aufgabe B: ");
		for(int i = 0; i <= 20; i++) {
			System.out.print(i * i + ", ");
		}
		
		System.out.println("");
		
		//Aufgabe c.)
		System.out.println("Aufgabe C: ");
		for(int i = 2; i <= 102; i += 4) {
			System.out.print(i + ", ");
		}
		
		System.out.println("");
		
		//Aufgabe d.)
		System.out.println("Aufgabe D: ");
		for(int i = 2; i <= 32; i++) {
			if(i % 2 == 0) {
				int answer = i * i;
				System.out.print(answer + ", ");
			}
		}

		System.out.println("");
		
		//Aufgabe e.)
		System.out.println("Aufgabe E: ");
		for(int i = 2; i < 32768; i++) {
			int answer = i ^ i;
			
			System.out.print(answer + ", ");
		}
	}
}
