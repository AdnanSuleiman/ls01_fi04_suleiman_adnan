﻿import java.util.Scanner;

class Fahrkartenautomat {

	//String
	private static String errorPrefix = "[Fehler] ";

	public static void main(String[] args) {
		double zuZahlenderBetrag; 
		double eingezahlterGesamtbetrag;

		while(true) {

			// Zu Zahlender Betrag
			// -------------------

			zuZahlenderBetrag = fahrkartenbestellungErfassen();

			// Geldeinwurf
			// -----------

			eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlenderBetrag);

			// Fahrscheinausgabe
			// -----------------

			fahrkartenAusgeben();

			// Rückgeldberechnung und -Ausgabe
			// -------------------------------

			rueckgeldAusgeben(eingezahlterGesamtbetrag, zuZahlenderBetrag);

			System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n" + "Wir wünschen Ihnen eine gute Fahrt.");
		}
	}

	public static void printBoard(String[] fahrkartenname, double[] fahrkartenpreis) {
		System.out.println("Fahrkartenbestellvorgang:");
		System.out.println("=========================");
		System.out.println("");
		System.out.println("Wählen Sie Ihren wunsch Tarif aus:");
		
		for(int i = 0; i < fahrkartenname.length; i++) {
			System.out.println(fahrkartenname[i] + " - " + fahrkartenpreis[i] + " €");
		}
		
		System.out.println("");
		System.out.println("Ihre Wahl: ");
	}
	
	public static double fahrkartenbestellungErfassen() {
		Scanner input = new Scanner(System.in);
		
		String[] fahrkartenname = new String[] {"1. Einzelfahrschein Berlin AB", "2. Einzelfahrschein Berlin BC", "3. Einzelfahrschein Berlin ABC", "4. Kurzstrecke", "5. Tageskarte Berlin AB", "6. Tageskarte Berlin BC", "7. Tageskarte Berlin ABC", "8. Kleingruppen-Tageskarte Berlin AB", "9. Kleingruppen-Tageskarte Berlin BC" , "10. Kleingruppen-Tageskarte Berlin ABC"};
		double[] fahrkartenpreise = new double[] {2.90, 3.30, 3.60, 1.90, 8.60, 9.00, 9.60, 23.50, 24.30, 24.90};
		
		printBoard(fahrkartenname, fahrkartenpreise);
		
		double zuZahlen = fahrkartenpreise[(input.nextInt() - 1)];
		
		System.out.print("Anzahl der Tickets: ");
		int[] anzahlTickets = new int[] {input.nextInt()};

		while(true) {
			if(anzahlTickets[0] < 1 || anzahlTickets[0] > 10) {
				System.out.println("[Fehler] Deine anzahl der Tickets wurde auf 1 gesetzt, da Sie mindestens 1 und höchstens 10 Tickets bestellen dürfen.");

				continue;
			}

			break;
		}

		return zuZahlen *= anzahlTickets[0];
	}

	public static double fahrkartenBezahlen(double zuZahlen) {
		Scanner input = new Scanner(System.in);

		double gezahlterGesamtbetrag = 0.0;

		while(gezahlterGesamtbetrag < zuZahlen) {
			System.out.printf("Noch zu zahlen: %.2f Euro \n", (zuZahlen - gezahlterGesamtbetrag));

			System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
			double eingeworfeneMünze = input.nextDouble();

			gezahlterGesamtbetrag += eingeworfeneMünze;
		}

		return gezahlterGesamtbetrag;
	}

	public static void fahrkartenAusgeben() {
		System.out.println("\nFahrschein wird ausgegeben");

		for (int i = 0; i < 8; i++){
			System.out.print("=");

			warte(250);
		}

		System.out.println("\n\n");
	}

	public static double rueckgeldAusgeben(double gezahlterGesamtbetrag, double zuZahlen) {
		double rueckBetrag = gezahlterGesamtbetrag - zuZahlen;

		if(rueckBetrag > 0.0){
			System.out.println("Der Rückgabebetrag in Höhe von " + rueckBetrag + " EURO");
			System.out.println("wird in folgenden Münzen ausgezahlt:");

			while(rueckBetrag >= 2.0) // 2 EURO-Münzen
			{
				muenzeAusgeben(2, "EURO");
				rueckBetrag -= 2.0;
			}
			while(rueckBetrag >= 1.0) // 1 EURO-Münzen
			{
				muenzeAusgeben(1, "EURO");
				rueckBetrag -= 1.0;
			}
			while(rueckBetrag >= 0.5) // 50 CENT-Münzen
			{
				muenzeAusgeben(50, "CENT");
				rueckBetrag -= 0.5;
			}
			while(rueckBetrag >= 0.2) // 20 CENT-Münzen
			{
				muenzeAusgeben(20, "CENT");
				rueckBetrag -= 0.2;
			}
			while(rueckBetrag >= 0.1) // 10 CENT-Münzen
			{
				muenzeAusgeben(10, "CENT");
				rueckBetrag -= 0.1;
			}
			while(rueckBetrag >= 0.05) // 5 CENT-Münzen
			{
				muenzeAusgeben(5, "CENT");
				rueckBetrag -= 0.05;
			}
		}

		return rueckBetrag;
	}

	public static void warte(int millisekunde) {
		try {
			Thread.sleep(millisekunde);
		}catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public static void muenzeAusgeben(int betrag, String einheit) {
		System.out.println(betrag + " " + einheit);
	}

//	public static double getCategory(int input) {
//		double einzelFahrausweis = 3.6;        
//		double tageskartenFahrausweis = 7.2;  
//		double gruppenFahrausweis = 22.0;      
//
//		switch(input) {
//		case 1:
//			return einzelFahrausweis;
//
//		case 2:
//			return tageskartenFahrausweis;
//
//		case 3:
//			return gruppenFahrausweis;	
//
//		default:
//			System.out.println(errorPrefix + "Verfügbare Fahrkarten: Einzelfahrausweis, Tageskarte, Gruppenkarte");
//
//			break;
//		}
//
//		return 0;
//	}
}