import java.util.Scanner;

public class Zaehlen {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		//Aufgabe a.)
		System.out.println("Gebe eine Zahl ein! (Aufsteigend)");
		int n = input.nextInt();

		for(int i = 0; i < n; i++) {
			System.out.println(i);
		}
		
		//Aufgabe b.)
		System.out.println("Gebe eine Zahl ein! (Absteigend)");
		int m = input.nextInt();
		
		for(int j = m; j != 0; j--) {
			System.out.println(j);
		}
	}
}
