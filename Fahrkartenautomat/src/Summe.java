import java.util.Scanner;

public class Summe {
	
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		System.out.println("Geben Sie bitte einen begrenzenden Wert ein: ");
		int max = input.nextInt();
		int answer = 0;
		
		//Aufgabe a.)
		for(int i = 0; i <= max; i++) {
			answer = (max * (i + 1)) / 2;
		}
		
		System.out.println("Die Summe f�r A betr�gt: " + answer);
		
		//Aufgabe b.)
		for(int i = 0; i <= max; i++) {
			answer = (max * (i + 2)) / 4;
		}
		
		System.out.println("Die Summe f�r B betr�gt: " + answer);
		
		//Aufgabe c.)
		for(int i = 4; i <= max; i++) {
			answer = (max * (i + 2)) / 5;
		}
		
		System.out.println("Die Summe f�r C betr�gt: " + answer);
	}
}
