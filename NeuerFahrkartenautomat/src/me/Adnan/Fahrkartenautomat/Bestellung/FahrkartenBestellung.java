package me.Adnan.Fahrkartenautomat.Bestellung;

import java.util.Scanner;

import me.Adnan.Fahrkartenautomat.main.Main;

public class FahrkartenBestellung {
	
	public static void fahrkartenbestellungErfassen() {
		Scanner input = new Scanner(System.in);
		
		/*
		 * Fahrkartenbestellung Aufnehmen
		 */
		
		String[] fahrkartenname = new String[] {"1. Einzelfahrschein Berlin AB", "2. Einzelfahrschein Berlin BC", "3. Einzelfahrschein Berlin ABC", "4. Kurzstrecke", "5. Tageskarte Berlin AB", "6. Tageskarte Berlin BC", "7. Tageskarte Berlin ABC", "8. Kleingruppen-Tageskarte Berlin AB", "9. Kleingruppen-Tageskarte Berlin BC" , "10. Kleingruppen-Tageskarte Berlin ABC"};
		double[] fahrkartenpreis = new double[] {2.90, 3.30, 3.60, 1.90, 8.60, 9.00, 9.60, 23.50, 24.30, 24.90};
		
		System.out.println("Fahrkartenbestellvorgang:");
		System.out.println("=========================");
		System.out.println("");
		System.out.println("W�hlen Sie Ihren wunsch Tarif aus:");
		
		for(int i = 0; i < fahrkartenname.length; i++) {
			System.out.println(fahrkartenname[i] + " - " + fahrkartenpreis[i] + " �");
		}
		
		System.out.println("");
		System.out.println("Ihre Wahl: ");
		
		/*
		 * Fahrkartentickets Erfassen
		 */
		
		double zuZahlen = fahrkartenpreis[(input.nextInt() - 1)];
		
		System.out.print("Anzahl der Tickets: ");
		int[] anzahlTickets = new int[] {input.nextInt()};

		while(true) {
			if(anzahlTickets[0] < 1 || anzahlTickets[0] > 10) {
				System.out.println(Main.errorPrefix + "Die Anzahl der Tickets darf mindestens 1 und h�chstens 10 Tickets bestellen d�rfen.");

				continue;
			}

			break;
		}
		
		zuZahlen *= anzahlTickets[0];
		
		
	}
}
