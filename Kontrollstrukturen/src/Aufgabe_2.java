
public class Aufgabe_2 {

	public static void main(String[] args) {
		//b.)
		String a = "F�r diesen Text wird eine Pr�fsumme berechnet.";
		String b = "F�r diesen Text wird auch eine Pr�fsumme berechnet.";
		String c = "Auch f�r diesen Text wird eine Pr�fsumme berechnet.";
		String d = "Und f�r diesen Text.";
		
		int sum = checksum(c);
		System.out.print(sum);

	}
	
	static int checksum(String s){
		int sum = 0;

		for (int i = 0; i < s.length(); i++){
			sum += s.charAt(i);
		}
		
		return sum;
	}
}
